from flask import Flask, render_template, request, url_for, flash,session
from werkzeug.utils import redirect
from flask_mysqldb import MySQL
from localStoragePy import localStoragePy
from flask_session import Session
from array import *
import pandas as pd
import numpy as np

app = Flask(__name__)
app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'a'

# app = Flask(__name__)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

mysql = MySQL(app)

# @app.route('/proseslogin', methods=['GET', 'POST'])

# def proseslogin():
#     if request.method == "POST":
#      cur = mysql.connection.cursor()
#      username = request.form['username']
#      password = request.form['password']
#      cur = mysql.connection.cursor()
#      cur.execute(
#             'SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
#      mysql.connection.commit()
#     #  cur.execute("SELECT * FROM dosen")
#     #  dataku = cur.fetchall()
#     #  cur.execute("SELECT * FROM kriteria")
#     account = cur.fetchone()
#     # #  datagabung = [dataku, data2]
#     #  cur.close()
#     # for index in range(len(datagabung)):
#     # print("index: %s | value: %s" % (index, datagabung[index]))
#     # data=str(account[1])

#     # print(data)
#     # for record in datagabung:
#     # print(record)
#     if account:
#             # Create session data, we can access this data in other routes
#             session['loggedin'] = True
#             session['id'] = str(account[0])
#             session['username'] = str(account[1])
#             # Redirect to home page
#             msg='Logged in successfully!'
#             return render_template('admin/index.html',msg=msg)
#     else:
#             # Account doesnt exist or username/password incorrect
#             msg = 'Incorrect username/password!'

#             return render_template('auth/Login.html',msg=msg)
    
# @app.route('/logout')
# def logout():
#     # Remove session data, this will log the user out
#    session.pop('loggedin', None)
#    session.pop('id', None)
#    session.pop('username', None)
#    # Redirect to login page
#    msg="Success Logout!"
#    return redirect(url_for('login'))

# @app.route('/', methods=['GET', 'POST'])
# def login():
#     # if request.method == "POST":
#     #  cur = mysql.connection.cursor()
#     #  username = request.form['username']
#     #  password = request.form['password']
#     # #  cur = mysql.connection.cursor()
#     # #  cur.execute(
#     # #         'SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
#     # #  mysql.connection.commit()
#     # # #  cur.execute("SELECT * FROM dosen")
#     # # #  dataku = cur.fetchall()
#     # # #  cur.execute("SELECT * FROM kriteria")
#     # #  data2 = cur.fetchall()
#     # # #  datagabung = [dataku, data2]
#     # #  cur.close()
#     # data=username
#     # # # for index in range(len(datagabung)):
#     # # # print("index: %s | value: %s" % (index, datagabung[index]))
#     # #  print(data2)
#     # # # for record in datagabung:
#     # print(data)

#     return render_template('auth/Login.html')
#     #return render_template('auth/Login.html')


# @app.route('/regist')
# def regist():
#     return render_template('auth/Regist.html')


@app.route('/')
@app.route('/', methods =['GET', 'POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        cursor = mysql.connection.cursor()
        cursor.execute('SELECT * FROM accounts WHERE username = % s AND password = % s', (username, password, ))
        kriteria = mysql.connection.cursor()
        kriteria.execute('SELECT COUNT(*) as kriteria FROM kriteria')
        result = kriteria.fetchone()
        total_rows = result[0]
        #
        dosen = mysql.connection.cursor()
        dosen.execute('SELECT COUNT(*) as kriteria FROM dosen')
        result2 = dosen.fetchone()
        total_rows2 = result2[0]
        #
        mahasiswa = mysql.connection.cursor()
        mahasiswa.execute('SELECT COUNT(*) as kriteria FROM mahasiswa')
        result3 = mahasiswa.fetchone()
        total_rows3 = result3[0]
        account = cursor.fetchone()
        if account:
            session['loggedin'] = True
            # session['id'] = account[1]['id']
            # session['username'] = account[2]['username']
            session['test']="account"
            msg = 'Logged in successfully !'
            return render_template('admin/index.html', msg = msg,data=total_rows,data2=total_rows2,data3=total_rows3)
        else:
            msg = 'Incorrect username / password !'
    return render_template('auth/Login.html', msg = msg)
 
@app.route('/logout')
def logout():
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    return redirect(url_for('login'))
 
# @app.route('/register', methods =['GET', 'POST'])
# def register():
#     msg = ''
#     if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form :
#         username = request.form['username']
#         password = request.form['password']
#         email = request.form['email']
#         cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
#         cursor.execute('SELECT * FROM accounts WHERE username = % s', (username, ))
#         account = cursor.fetchone()
#         if account:
#             msg = 'Account already exists !'
#         elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
#             msg = 'Invalid email address !'
#         elif not re.match(r'[A-Za-z0-9]+', username):
#             msg = 'Username must contain only characters and numbers !'
#         elif not username or not password or not email:
#             msg = 'Please fill out the form !'
#         else:
#             cursor.execute('INSERT INTO accounts VALUES (NULL, % s, % s, % s)', (username, password, email, ))
#             mysql.connection.commit()
#             msg = 'You have successfully registered !'
#     elif request.method == 'POST':
#         msg = 'Please fill out the form !'
#     return render_template('register.html', msg = msg)


@app.route('/insertaccount', methods=['GET','POST'])
def insertaccount():
    msg=''
    if request.method == "POST":
        flash("Data Inserted Successfully")
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        # nilai_str = ",".join(nilai)
        cur = mysql.connection.cursor()
        cur.execute(
            "INSERT INTO accounts (username, password, email) VALUES (%s, %s, %s)", (username, password, email))
        mysql.connection.commit()
    return render_template('auth/Login.html', msg = msg)

@app.route('/regist', methods=['GET'])
def regist():
   
    return render_template('auth/Regist.html')


@app.route('/dashboard')
def dashboard():
    return render_template('admin/index.html')

Pokemons =["Pikachu", "Charizard", "Squirtle", "Jigglypuff", 
           "Bulbasaur", "Gengar", "Charmander", "Mew", "Lugia", "Gyarados"]
@app.route('/spk')
def spk():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM dosen")
    dataku = cur.fetchall()
    cur.execute("SELECT * FROM kriteria")
    data2 = cur.fetchall()
    datagabung = [dataku, data2]
    cur.close()
    # for index in range(len(datagabung)):
    # print("index: %s | value: %s" % (index, datagabung[index]))
    print(datagabung)
    # for record in datagabung:
    # print(record)

    return render_template('spk/index.html', len = len(dataku), data = dataku ,len2 = len(data2),data2=data2)

@app.route('/insertmasterspk', methods=['POST'])
def insertmasterspk():
    if request.method == "POST":
        flash("Data Inserted Successfully")
    data = [
        [0, -1, -1, -2, 2, 2, 3],
        [3, -3, -2, 1, -1, -3, -3],
        [3, 2, 2, -1, -1, -1, 2],
        [-2, -3, 3, -2, -3, -3, -2],
        [-2, -3, -2, -1, -2, -3, 2],
        [3, 3, -2, -2, 3, -2, -2],
        [-2, 2, -1, -4, 2, 2, -2],
        [0, 1, 1, 1, 1, -1, 2],
        [0, 2, 2, -2, -2, 2, 2],
        [-2, -2, -2, 3, -2, -2, -2],
        [0, -1, -3, -4, 2, -1, -3]
    ]

    bobot = {
        0: 5,
        1: 4.5,
        -1: 4,
        2: 3.5,
        -2: 3,
        3: 2.5,
        -3: 2,
        4: 1.5,
        -4: 1
    }

    dosen = ["Dr. Feri Candra, S.T., M.T.", "Dr. Dahliyusmanto S.Kom., M.Sc.", "Dr. Irsan Taufik Ali, S.T., M.T.", "Noveri Lysbetti Marpaung, S.T., M.Sc.", "Rahyul Amri, S.T., M.T.", "Linna Oktaviana Sari, S.T., M.T.", "Salhazan Nasution, S.Kom., MIT.", "T. Yudi Hadiwandra, S.Kom., M.Kom.", "Rahmat Rizal Andhi, S.T., M.T.", "Edi Susilo, S.Pd., M.Kom., M.Eng.", "Dian Ramadhani, S.T., M.T."]

    new_data = []
    for row in data:
        new_row = []
        for value in row:
            if value in bobot:
                new_value = bobot[value]
                new_row.append(new_value)
            else:
                new_row.append(value)
        new_data.append(new_row)

    core_factor = [row[0:3] for row in new_data]
    secondary_factor = [row[3:] for row in new_data]

    results = []
    for i, (core_values, secondary_values) in enumerate(zip(core_factor, secondary_factor)):
        nilai_corefactor = sum(core_values) / len(core_values)
        nilai_secondaryfactor = sum(secondary_values) / len(secondary_values)
        total_nilai = (0.6 * nilai_corefactor) + (0.4 * nilai_secondaryfactor)
        bulat = round(total_nilai, 2)

        result = {
            "dosen": dosen[i],
            "core_factor": core_values,
            "secondary_factor": secondary_values,
            "nilai_corefactor": nilai_corefactor,
            "nilai_secondaryfactor": nilai_secondaryfactor,
            "total_nilai": total_nilai,
            "total_nilai_bulat": bulat
        }

        results.append(result)

    return render_template('spk/hasil.html', results=results, data=data)


@app.route('/insertokritiksaran', methods=['POST'])
def insertokritiksaran():
    if request.method == "POST":
        flash("Data Inserted Successfully")
        kritik = request.form['kritik']
        saran = request.form['saran']
        nilai = request.form['nilai']
        peringkat = request.form['peringkat']
        judul_skripsi = request.form['judul_skripsi']
        # nilai_str = ",".join(nilai)
        cur = mysql.connection.cursor()
        cur.execute(
            "INSERT INTO kritik_dan_saran (kritik, saran, nilai, peringkat,judul_skripsi) VALUES (%s, %s, %s, %s, %s)", (kritik, saran, nilai, peringkat,judul_skripsi))
        mysql.connection.commit()
        return redirect(url_for('spk'))




@app.route('/mahasiswa')
def mahasiswa():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM mahasiswa")
    data = cur.fetchall()
    cur.close()
    print(data)

    return render_template('mahasiswa/index.html', mahasiswa=data)


@app.route('/dosen')
def dosen():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM dosen")
    data = cur.fetchall()
    cur.close()

    return render_template('dosen/index.html', dosen=data)


@app.route('/insert', methods=['POST'])
def insert():
    if request.method == "POST":
        flash("Data Inserted Successfully")
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        cur = mysql.connection.cursor()
        cur.execute(
            "INSERT INTO mahasiswa (name, email, phone) VALUES (%s, %s, %s)", (name, email, phone))
        mysql.connection.commit()
        return redirect(url_for('mahasiswa'))


@app.route('/insertdosen', methods=['POST'])
def insertdosen():
    if request.method == "POST":
        flash("Data Inserted Successfully")
        nama = request.form['nama']
        univ = request.form['univ']
        jurusan = request.form['jurusan']
        cur = mysql.connection.cursor()
        cur.execute(
            "INSERT INTO dosen (nama, univ, jurusan) VALUES (%s, %s, %s)", (nama, univ, jurusan))
        mysql.connection.commit()
        return redirect(url_for('dosen'))


@app.route('/delete/<string:id_data>', methods=['GET'])
def delete(id_data):
    flash("Record Has Been Deleted Successfully")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM mahasiswa WHERE id=%s", (id_data,))
    mysql.connection.commit()
    return redirect(url_for('mahasiswa'))


@app.route('/deletedosen/<string:id_data>', methods=['GET'])
def deletedosen(id_data):
    flash("Record Has Been Deleted Successfully")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM dosen WHERE dosen_id=%s", (id_data,))
    mysql.connection.commit()
    return redirect(url_for('dosen'))


@app.route('/update', methods=['POST', 'GET'])
def update():
    if request.method == 'POST':
        id_data = request.form['id']
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']

        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE mahasiswa SET name=%s, email=%s, phone=%s
        WHERE id=%s
        """, (name, email, phone, id_data))
        flash("Data Updated Successfully")
        return redirect(url_for('mahasiswa'))


@app.route('/updatedosen', methods=['POST', 'GET'])
def updatedosen():
    if request.method == 'POST':
        id_data = request.form['id']
        nama = request.form['nama']
        univ = request.form['univ']
        jurusan = request.form['jurusan']

        cur = mysql.connection.cursor()
        cur.execute("""UPDATE dosen SET nama=%s, univ=%s, jurusan=%s WHERE dosen_id=%s""",
                    (nama, univ, jurusan, id_data))
        flash("Data Updated Successfully")
        return redirect(url_for('dosen'))


@app.route('/kriteria')
def kriteria():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM kriteria")
    data = cur.fetchall()
    cur.close()

    return render_template('kriteria/index.html', kriteria=data)


@app.route('/insertkriteria', methods=['POST'])
def insertkriteria():
    if request.method == "POST":
        flash("Data Inserted Successfully")
        nama = request.form['nama_kriteria']
        jenis = request.form['jenis_kriteria']
        cur = mysql.connection.cursor()
        cur.execute(
            "INSERT INTO kriteria (nama_kriteria, jenis_kriteria) VALUES (%s, %s)", (nama, jenis))
        mysql.connection.commit()
        return redirect(url_for('kriteria'))


@app.route('/updatekriteria', methods=['POST', 'GET'])
def updatekriteria():
    if request.method == 'POST':
        id_data = request.form['id']
        nama = request.form['nama_kriteria']
        jenis = request.form['jenis_kriteria']

        cur = mysql.connection.cursor()
        cur.execute(
            """UPDATE kriteria SET nama_kriteria=%s, jenis_kriteria=%s WHERE id=%s""", (nama, jenis, id_data))
        flash("Data Updated Successfully")
        return redirect(url_for('kriteria'))


@app.route('/deletekriteria/<string:id_data>', methods=['GET'])
def deletekriteria(id_data):
    flash("Record Has Been Deleted Successfully")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM kriteria WHERE id=%s", (id_data,))
    mysql.connection.commit()
    return redirect(url_for('kriteria'))


if __name__ == "__main__":
    app.run(debug=True)
