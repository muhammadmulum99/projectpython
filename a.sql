-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2023 at 11:13 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `a`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', 'admin', 'admin@mail.com');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `dosen_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `univ` varchar(255) NOT NULL,
  `jurusan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`dosen_id`, `nama`, `univ`, `jurusan`) VALUES
(1, 'Dr. Feri Candra, S.T., M.T.', 'dosen', 'dosen'),
(2, 'Dr. Dahliyusmanto S.Kom., M.Sc.', 'aaa', 'aaaa'),
(3, 'Dr. Irsan Taufik Ali, S.T., M.T.', 'dosen', 'dosen'),
(4, 'Noveri Lysbetti Marpaung, S.T., M.Sc.', 'aaa', 'aaaa'),
(5, 'Rahyul Amri, S.T., M.T.', 'aaa', 'aaaa'),
(6, 'Linna Oktaviana Sari, S.T., M.T.', 'dosen', 'dosen'),
(7, 'Salhazan Nasution, S.Kom., MIT.', 'aaa', 'aaaa'),
(8, 'T. Yudi Hadiwandra, S.Kom., M.Kom.', 'aaa', 'aaaa'),
(9, 'Rahmat Rizal Andhi, S.T., M.T.', 'aaa', 'aaaa'),
(10, 'Edi Susilo, S.Pd., M.Kom., M.Eng.', 'dosen', 'dosen'),
(11, 'Dian Ramadhani, S.T., M.T.', 'aaa', 'aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int(11) NOT NULL,
  `nama_kriteria` varchar(255) NOT NULL,
  `jenis_kriteria` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `nama_kriteria`, `jenis_kriteria`) VALUES
(4, 'Kosentrasi', 'Kriteria Mahasiswa'),
(5, 'Mata Kuliah Terkait', 'Kriteria Mahasiswa'),
(6, 'Nilai Mata Kuliah Terkait', 'Kriteria Mahasiswa'),
(7, 'Kouta', 'Kriteria Dosen'),
(8, 'Bidang Ahli', 'Kriteria Dosen'),
(9, 'Mata Kuliah Yang pernah di ampu', 'Kriteria Dosen'),
(10, 'Penelitian yang pernah dilakukan', 'Kriteria Dosen');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_jenis`
--

CREATE TABLE `kriteria_jenis` (
  `id` int(11) NOT NULL,
  `nama_jenis_kriteria` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kriteria_jenis`
--

INSERT INTO `kriteria_jenis` (`id`, `nama_jenis_kriteria`) VALUES
(1, 'Kriteria Dosen'),
(2, 'Kriteria Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `kritik_dan_saran`
--

CREATE TABLE `kritik_dan_saran` (
  `id` int(11) NOT NULL,
  `kritik` text NOT NULL,
  `saran` text NOT NULL,
  `nilai` text NOT NULL,
  `peringkat` text NOT NULL,
  `judul_skripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `name`, `email`, `phone`) VALUES
(1, 'test', 'test', 'test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`dosen_id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria_jenis`
--
ALTER TABLE `kriteria_jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kritik_dan_saran`
--
ALTER TABLE `kritik_dan_saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `dosen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kriteria_jenis`
--
ALTER TABLE `kriteria_jenis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kritik_dan_saran`
--
ALTER TABLE `kritik_dan_saran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
